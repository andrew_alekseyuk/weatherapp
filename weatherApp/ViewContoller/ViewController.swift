import UIKit
import MapKit
//import CoreLocation
import SpriteKit


class ViewController: UIViewController, UISearchBarDelegate, MKLocalSearchCompleterDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsTable: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var loactionLabel: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunriseValue: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var sunsetValue: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var pressureValue: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var humidityValue: UILabel!
    @IBOutlet weak var windspeedLabel: UILabel!
    @IBOutlet weak var windspeedValue: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var visibilityValue: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var lastUpdatedValue: UILabel!
    
    @IBOutlet weak var hourlyWeather: UICollectionView!
    @IBOutlet weak var dailyWeather: UITableView!
    @IBOutlet weak var effectsView: UIView!
    @IBOutlet weak var backgroudImage: UIImageView!
    @IBOutlet weak var backgroundImageTwo: UIImageView!
    
    
    let rain = SKScene(fileNamed: "Rain.sks")!
    let snow = SKScene(fileNamed: "Snow.sks")!
    let skView = SKView()
    
    
    var viewModel: ViewModel?
    
    var currentWeather:CurrentWeather?
    var hourlyWeatherArray:[HourlyForecast] = []
    var dailyWeatherArray:[DailyForecast] = []
    
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Manager.shared.getLangauge()
        setUpSearch()
        self.viewModel = ViewModel()
        viewModel?.startUpdateLocation()
        bindOutlitsCurrentWeather()
        bindOutlitsForecastWeather()
        bindSetBackgroundAndEffects()
        backgroudImage.addParalaxEffect()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        spinner.startAnimating()
        
    }
    
    func setUpSearch () {
        searchCompleter.delegate = self
        searchBar?.delegate = self
        searchResultsTable?.delegate = self
        searchResultsTable?.dataSource = self
        searchResultsTable.isHidden = true
    }
    
    func bindSetBackgroundAndEffects () {
        
        self.viewModel?.weatherTypeInEnglish.bind({ (weatherType) in
            guard let weatherTypeUnwrapped = weatherType else {return}
            switch weatherTypeUnwrapped {
            case "clear sky":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "clear sky")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "clear sky")
                }
            case "few clouds":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "few clouds")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "few clouds")
                }
            case "scattered clouds":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "scattered clouds")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "scattered clouds")
                }
            case "broken clouds":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "broken_clouds")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "broken_clouds")
                }
            case "overcast clouds":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "broken_clouds")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "broken_clouds")
                }
            case "shower rain":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "shower rain")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "rain")
                }
                self.addRain()
            case "moderate rain":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "shower rain")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "rain")
                }
                self.addRain()
            case "light rain":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "rain")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "rain")
                }
                self.addRain()
            case "rain":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "rain")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "rain")
                }
                self.addRain()
            case "thunderstorm":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "thunderstorm")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "thunderstorm")
                }
                self.addRain()
            case "snow"  :
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "snow")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "snow")
                }
                self.addSnow()
            case "light snow"  :
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "snow")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "snow")
                }
                self.addSnow()
            case "mist":
                self.removeEffects()
                self.backgroudImage.image = UIImage(named: "mist")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                } completion: { (bool) in
                    self.backgroundImageTwo.image = UIImage(named: "mist")
                }
            default:
                self.backgroudImage.image = UIImage(named: "clear sky")
                self.backgroudImage.alpha = 0
                UIView.animate(withDuration: 1) {
                    self.backgroudImage.alpha = 1
                }
            }
        })
    }
    
    
    private func bindOutlitsCurrentWeather() {
        self.viewModel?.location.bind({ (location) in
            guard let locationUnwrapped = location else {return}
            self.loactionLabel?.text = "\(locationUnwrapped)"
        })
        self.viewModel?.weatherType.bind({ (weatherType) in
            self.weatherType?.text = weatherType
        })
        self.viewModel?.temperature.bind({ (temperature) in
            guard let temperatureUnwrapped = temperature else {return}
            self.temperature?.text = "\(temperatureUnwrapped)" + "°"
        })
        self.viewModel?.feelsLike.bind({ (feelsLike) in
            guard let feelsLikeUnwrapped = feelsLike else {return}
            self.feelsLike?.text = "feels like".localized + " \(feelsLikeUnwrapped)" + "°"
        })
        self.viewModel?.sunrise.bind({ (sunrise) in
            guard let sunriseUnwrapped = sunrise else {return}
            let humanReadable = Date(timeIntervalSince1970: TimeInterval(sunriseUnwrapped))
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH:mm"
            let humanReadableFormated = timeFormatter.string(from: humanReadable)
            self.sunriseLabel.text = "Sunrise".localized
            self.sunriseValue.text = humanReadableFormated
        })
        self.viewModel?.sunset.bind({ (sunset) in
            guard let sunsetUnwrapped = sunset else {return}
            let humanReadable = Date(timeIntervalSince1970: TimeInterval(sunsetUnwrapped))
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH:mm"
            let humanReadableFormated = timeFormatter.string(from: humanReadable)
            self.sunsetLabel.text = "Sunset".localized
            self.sunsetValue.text = humanReadableFormated
        })
        self.viewModel?.pressure.bind({ (pressure) in
            guard let pressureUnwrapped = pressure else {return}
            self.pressureLabel.text = "Pressure".localized
            self.pressureValue.text = "\(pressureUnwrapped)" + " hPa".localized
        })
        self.viewModel?.humidity.bind({ (humidity) in
            guard let humidityUnwrapped = humidity else {return}
            self.humidityLabel.text = "Humidity".localized
            self.humidityValue.text = "\(humidityUnwrapped)" + " %"
        })
        self.viewModel?.windSpeed.bind({ (windSpeed) in
            guard let windSpeedUnwrapped = windSpeed else {return}
            self.windspeedLabel.text = "Wind Speed".localized
            self.windspeedValue.text = "\(windSpeedUnwrapped)" + " km/h".localized
        })
        self.viewModel?.visibility.bind({ (visibility) in
            guard let visibilityUnwrapped = visibility else {return}
            self.visibilityLabel.text = "Visibility".localized
            self.visibilityValue.text = "\(visibilityUnwrapped)" + " m".localized
        })
        self.viewModel?.lastUpdated.bind({ (lastUpdated) in
            let date = Date()
            let dateFormmater = DateFormatter()
            dateFormmater.timeZone = TimeZone.current
            dateFormmater.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.lastUpdatedLabel.text = "Updated".localized
            self.lastUpdatedValue.text = "\(dateFormmater.string(from: date))"
            self.spinner.stopAnimating()
        })
    }
    
    private func bindOutlitsForecastWeather() {
        self.viewModel?.hourlyWeatherArray.bind({ (result) in
            guard let resultUnwrapped = result else {return}
            self.hourlyWeatherArray = resultUnwrapped
            self.hourlyWeather.reloadData()
            self.spinner.stopAnimating()
        })
        self.viewModel?.dailyWeatherArray.bind({ (result) in
            guard let resultUnwrapped = result else {return}
            self.dailyWeatherArray = resultUnwrapped
            self.dailyWeather.reloadData()
            self.spinner.stopAnimating()
        })
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchResultsTable.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("clicked")
        self.spinner.startAnimating()
        searchCompleter.queryFragment = searchText
    }
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        searchResultsTable.reloadData()
    }
    
    // This method is called when there was an error with the searchCompleter
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // Error
    }
    private func addRain() {
        skView.frame = effectsView.frame
        skView.backgroundColor = .clear
        let scene = SKScene(size: effectsView.frame.size)
        scene.backgroundColor = .clear
        skView.presentScene(scene)
        skView.isUserInteractionEnabled = false
        scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        scene.addChild(rain)
        rain.position.y = scene.frame.maxY
        //emitterNode.particlePositionRange.dx = scene.frame.width
        effectsView.addSubview(skView)
        
    }
    private func addSnow() {
        skView.frame = effectsView.frame
        skView.backgroundColor = .clear
        let scene = SKScene(size: effectsView.frame.size)
        scene.backgroundColor = .clear
        skView.presentScene(scene)
        skView.isUserInteractionEnabled = false
        scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        scene.addChild(snow)
        snow.position.y = scene.frame.maxY
        //emitterNode.particlePositionRange.dx = scene.frame.width
        effectsView.addSubview(skView)
    }
    
    private func removeEffects() {
        skView.removeFromSuperview()
    }
    
    @IBAction func getCurrentLocationButton(_ sender: UIButton) {
        self.spinner.startAnimating()
        self.searchResultsTable.isHidden = true
        view.endEditing(true)
        viewModel?.startUpdateLocation()
        searchBar.text = ""
    }
    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchResultsTable {
            return searchResults.count
        }
        else  {
            return dailyWeatherArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == dailyWeather {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DailyTableViewCell", for: indexPath) as? DailyTableViewCell else {
                return UITableViewCell()
            }
            cell.configure(object: dailyWeatherArray[indexPath.row])
            return cell
        } else  {
            let searchResult = searchResults[indexPath.row]
            searchCompleter.pointOfInterestFilter = .excludingAll
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
            cell.textLabel?.text = searchResult.title
            cell.detailTextLabel?.text = searchResult.subtitle
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == searchResultsTable {
            tableView.deselectRow(at: indexPath, animated: true)
            let result = searchResults[indexPath.row]
            let searchRequest = MKLocalSearch.Request(completion: result)
            searchRequest.pointOfInterestFilter = .excludingAll
            
            let search = MKLocalSearch(request: searchRequest)
            
            search.start { (response, error) in
                
                guard let coordinate = response?.mapItems[0].placemark.coordinate else {
                    return
                }
                
                guard let name = response?.mapItems[0].name else {
                    return
                }
                
                print(name)

                self.searchBar.text = name
                self.searchBar.resignFirstResponder()
                self.searchResultsTable.isHidden = true
                self.viewModel?.getForecastWeather(lat: coordinate.latitude, lon: coordinate.longitude)
                self.viewModel?.getCurrentWeather(lat: coordinate.latitude, lon: coordinate.longitude)
                self.viewModel?.getWeatherInEnglish(lat: coordinate.latitude, lon: coordinate.longitude)
            }
            
        } else if tableView == dailyWeather {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourlyWeatherArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyCollectionViewCell", for: indexPath) as? HourlyCollectionViewCell else {
            return UICollectionViewCell ()
        }
        cell.configure(object: hourlyWeatherArray[indexPath.item])
        return cell
    }  
}



