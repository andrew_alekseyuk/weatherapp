import Foundation
import UIKit
import MapKit
import CoreLocation

class ViewModel: NSObject, CLLocationManagerDelegate {
    private let locationManager = CLLocationManager()
    
    
    var location = Bindable<String?>(nil)
    var weatherType = Bindable<String?>(nil)
    var temperature = Bindable<Double?>(nil)
    var feelsLike = Bindable<Double?>(nil)
    var sunrise = Bindable<Int?>(nil)
    var sunset = Bindable<Int?>(nil)
    var pressure = Bindable<Int?>(nil)
    var humidity = Bindable<Int?>(nil)
    var windSpeed = Bindable<Double?>(nil)
    var visibility = Bindable<Int?>(nil)
    var lastUpdated = Bindable<Int?>(nil)
    var weatherTypeInEnglish = Bindable<String?>(nil)
    
    var hourlyWeatherArray = Bindable<[HourlyForecast]?>(nil)
    var dailyWeatherArray = Bindable<[DailyForecast]?>(nil)

    
    func getCurrentWeather (lat:Double, lon:Double) {
            let lanFormatted = "lat=\(lat)"
            let lonFormatted = "&lon=\(lon)"
    
            Manager.shared.getCurrentWeather(lan: lanFormatted, lon: lonFormatted) { [weak self] (currentWeather) in
    
                DispatchQueue.main.async {
                    self?.location.value = currentWeather.name
                    self?.weatherType.value = currentWeather.weather?[0].description
                    self?.temperature.value = currentWeather.main?.temp
                    self?.feelsLike.value = currentWeather.main?.feels_like
                    self?.sunrise.value = currentWeather.sys?.sunrise
                    self?.sunset.value = currentWeather.sys?.sunset
                    self?.pressure.value = currentWeather.main?.pressure
                    self?.humidity.value = currentWeather.main?.humidity
                    self?.windSpeed.value = currentWeather.wind?.speed
                    self?.visibility.value = currentWeather.visibility
                    self?.lastUpdated.value = currentWeather.dt
                }
            }
        }
    
    func getForecastWeather (lat:Double, lon:Double) {
        
        let lanFormatted = "lat=\(lat)"
        let lonFormatted = "&lon=\(lon)"
        
        Manager.shared.getForecastWeather(lan: lanFormatted, lon: lonFormatted) { [weak self] (forecastWeather) in
            DispatchQueue.main.async {
                self?.hourlyWeatherArray.value = forecastWeather.hourly!
                self?.dailyWeatherArray.value = forecastWeather.daily!
            }
        }
    }
    
    func getWeatherInEnglish (lat:Double, lon:Double) {
        
        let lanFormatted = "lat=\(lat)"
        let lonFormatted = "&lon=\(lon)"
        
        Manager.shared.getCurrentWeatherInEnglish(lan: lanFormatted, lon: lonFormatted) { [weak self] (currentWeather) in
            DispatchQueue.main.async {
                self?.weatherTypeInEnglish.value = currentWeather.weather?[0].description
            }
        }
    }
    
    func startUpdateLocation () {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.pausesLocationUpdatesAutomatically = true
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(location.latitude) \(location.longitude)")
        getCurrentWeather(lat: location.latitude, lon: location.longitude)
        getForecastWeather(lat: location.latitude, lon: location.longitude)
        getWeatherInEnglish(lat: location.latitude, lon: location.longitude)
        getWeatherInEnglish(lat: location.latitude, lon: location.longitude)
        locationManager.stopUpdatingLocation()
    }
    
    
}
