import Foundation
import UIKit


class Manager {
    static let shared = Manager()
    
    private init() {}
    
    let apiURL = "https://api.openweathermap.org/data/2.5/"
    let currentWeatherSuffix = "weather?"
    let forecastWeatherSuffix = "onecall?exclude=minutely&"
    let key = "&appid=933feb1376f96226595f5a0c52d37cfc"
    let units = "&units=metric"
    var deviceLanguage = "&lang="
    
    func getCurrentWeatherInEnglish (lan: String, lon: String, completion: @escaping (CurrentWeather)->()) {
        guard let requestURL = URL(string: apiURL + currentWeatherSuffix + lan + lon  + units + "&lang=en" + key) else {return}
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let recievedData = data {
                do {
                    let currentWeather = try JSONDecoder().decode(CurrentWeather.self, from: recievedData)
                    print("OK")
                    completion(currentWeather)
                    
                } catch let error {
                    print(error)
                }
            }
        }
        task.resume()
        
    }
    
    
    func getCurrentWeather (lan: String, lon: String, completion: @escaping (CurrentWeather)->()) {
        guard let requestURL = URL(string: apiURL + currentWeatherSuffix + lan + lon  + units + deviceLanguage + key) else {return}
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let recievedData = data {
                do {
                    let currentWeather = try JSONDecoder().decode(CurrentWeather.self, from: recievedData)
                    print("OK")
                    completion(currentWeather)
                    
                } catch let error {
                    print(error)
                }
            }
        }
        task.resume()
        
    }
    
    func getForecastWeather (lan: String, lon: String, completion: @escaping (Forecast)->()) {
        guard let requestURL = URL(string: apiURL + forecastWeatherSuffix + lan + lon  + units + deviceLanguage + key) else {return}
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let recievedData = data {
                do {
                    let forecastWeather = try JSONDecoder().decode(Forecast.self, from: data!)
                    completion (forecastWeather)
                } catch let error {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    func getLangauge () {
        guard let langStr = Locale.current.languageCode else {return}
        deviceLanguage = deviceLanguage + langStr
    }
}
