import UIKit

class DailyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dailyIcon: UIImageView!
    @IBOutlet weak var dayTemp: UILabel!
    @IBOutlet weak var nightTemp: UILabel!
    
    let imageOne = UIImage()
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//    }
    
    func configure (object: DailyForecast) {
        let date = Date(timeIntervalSince1970: TimeInterval(object.dt!))
        let dayOfWeek = Calendar.current.component(.weekday, from: date)
        let dayOfWeekString = defineDayOfWeek(dayInt: dayOfWeek)
        dayLabel.text = String(dayOfWeekString)
        
        
        if let icon = object.weather?[0].icon {
            let URLstring = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
            let url = URL(string: URLstring)
            dailyIcon.loadDaily(url: url!)
            dailyIcon.contentMode = .scaleAspectFill
            dailyIcon.image = imageOne
            
            dayTemp.text = String(object.temp!.day!) + "°"
            nightTemp.text = String(object.temp!.night!) + "°"
            
        }
    }
    func defineDayOfWeek (dayInt: Int) -> String {
        switch dayInt {
        case 1:
            return "Monday".localized
        case 2:
            return "Tuesday".localized
        case 3:
            return "Wednesday".localized
        case 4:
            return "Thursday".localized
        case 5:
            return "Friday".localized
        case 6:
            return "Saturday".localized
        case 7:
            return "Sunday".localized
        
        default:
            print ("error defining dayOfWeek")
            return ""
        }
    }
}
extension UIImageView {
    func loadDaily(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self!.image = image
                        
                    }
                }
            }
        }
    }
}
