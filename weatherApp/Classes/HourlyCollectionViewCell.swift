import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    var image = UIImage()
    
    func configure(object: HourlyForecast) {
        guard let epochtime = object.dt else {return}
        let time = Date(timeIntervalSince1970: TimeInterval(epochtime))
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        
        let timeString = formatter.string(from: time)
        timeLabel.text = timeString
        
        
        if let icon = object.weather?[0].icon {
            let URLstring = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
            let url = URL(string: URLstring)
            imageView.load(url: url!)
            imageView.contentMode = .scaleAspectFill
            imageView.image = image
            
            
        guard let temperature = object.temp else {return}
        
        temperatureLabel.text = String(Int(temperature)) + "°"
        
    }
}


}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self!.image = image
                        
                    }
                }
            }
        }
    }
}
